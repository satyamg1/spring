package com.satyam.jdbc;

import java.util.List;
import java.util.Scanner;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

import com.satyam.jdbc.dao.Studentdao;
import com.satyam.jdbc.entities.Student;

public class App 
{
    public static void main( String[] args )
    {
       System.out.println("Program Strarted");
      //spring jdbc =>jdbc template
       
       ApplicationContext con= 
    		   new ClassPathXmlApplicationContext("com/satyam/jdbc/config.xml");
       
    	Studentdao studentdao = con.getBean("studentdaoimp",Studentdao.class);
    	
    	
    	//insert
//    	
//    	Student student = new Student ();
//    	
//    	student.setAddress("nagpur");
//    	student.setId(5);
//    	student.setName("aman");
//    	
//      int result = studentdao.insert(student);
//    	
//    	System.out.println("number of recored inserted  "+result);
    	
    	
    	//update
//    	Student student = new Student ();
//    	student.setId(5);
//    	student.setName("dobii");
//    	student.setAddress("delhi")
 //    	int r=studentdao.update(student);
 //     System.out.println("no. of rows updated"+ r);
    	
    	//delete
//    Scanner sc= new Scanner(System.in); 
//    System.out.println("enter the id you want to delete");
//    int a= sc.nextInt();  
//    int r=	studentdao.delete(a);
//    System.out.println("no. of rows deleted"+ r);
    	
    	//select on row
//    	Student student1 =studentdao.select(1);
//    	System.out.println(student1);
    	
    	//select whole rows
    	
    	List<Student> students = studentdao.allstudent();
    	for(Student s: students)
    	{
    		System.out.println(s);
    	}
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    }
}
