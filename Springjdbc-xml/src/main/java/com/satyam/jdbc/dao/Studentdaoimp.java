package com.satyam.jdbc.dao;

import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.satyam.jdbc.entities.Student;

public class Studentdaoimp implements Studentdao {

	
	private JdbcTemplate  jdbctemplate; 
	
	public JdbcTemplate getJdbctemplate() {
		return jdbctemplate;
	}
	
	public void setJdbctemplate(JdbcTemplate jdbctemplate) {
		this.jdbctemplate = jdbctemplate;
	}

	public int insert(Student student) {
		
		String query= "insert into student(id,name,address) values(?,?,?)";
	int r=	this.jdbctemplate.update(query,student.getId(),student.getName(),student.getAddress());
    return r;
	}

	public int update(Student student) {
		
		String query = "update student set name =?,address=? where id=?";
		int u=this.jdbctemplate.update(query,student.getName(),student.getAddress(),student.getId());
		
		return u;
	}

	public int delete(int id) {
		
		String query="delete from student where id=?";
		int r=this.jdbctemplate.update(query,id);
		
		
		return r;
	}

	public Student select(int id) {
		
		String query="select * from student where id=?";
		RowMapper<Student>rowmapper =new RowMapperimp();
	Student student=	this.jdbctemplate.queryForObject(query,rowmapper, id);
	
	
		
		
		return student;
	}

	public List<Student> allstudent() {
		
		String query="select * from student";
		RowMapper<Student>rowmapper =new RowMapperimp();
		List<Student> student =this.jdbctemplate.query(query,rowmapper );
		
		
		
		return student;
	}

	
	}


