package com.satyam.jdbc.dao;

import java.util.List;

import com.satyam.jdbc.entities.Student;

public interface Studentdao {
	
	public int insert(Student student);
	public int update(Student student);
	public int delete(int id);
	public Student select(int id);
	public List<Student> allstudent();
	

}
