package com.satyam.core.Sprinspel;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Demo {
	@Value("#{22+11}")
	private int x;
	@Value("#{8>6?88:99}")
	private int y;
	@Value("#{T(java.lang.Math).sqrt(20)}")
	private double z;
	@Value("#{T(java.lang.Math).PI}")
	private double pi;
	@Value("#{new java.lang.String('rgwrgw4rg')}")
	private String name;
	@Value("#{9>5}")
	private boolean active;
	
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getPi() {
		return pi;
	}
	public void setPi(double pi) {
		this.pi = pi;
	}
	public double getZ() {
		return z;
	}
	public void setZ(double z) {
		this.z = z;
	}
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	/**
	 * 
	 */
	public Demo() {
		super();
		// TODO Auto-generated constructor stub
	}
	

	

	
	@Override
	public String toString() {
		return "Demo [x=" + x + ", y=" + y + ", z=" + z + ", pi=" + pi + ", name=" + name + ", active=" + active + "]";
	}
	/**
	 * @param x
	 * @param y
	 */
	public Demo(int x, int y) {
		super();
		this.x = x;
		this.y = y;
	}
	
	
	

}
