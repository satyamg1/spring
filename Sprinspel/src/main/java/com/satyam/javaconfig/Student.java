package com.satyam.javaconfig;

import org.springframework.stereotype.Component;


public class Student {
	
	private Samosa samosa;
	
	public void study()
	{
		((Samosa) this.samosa).display();
		 System.out.println("student is study");
	}

	public Samosa getSamosa() {
		return samosa;
	}

	public void setSamosa(Samosa samosa) {
		this.samosa = samosa;
	}

	
	/**
	 * @param samosa
	 */
	public Student(Samosa samosa) {
		super();
		this.samosa = samosa;
	}

	

}
