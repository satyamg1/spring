package com.satyam.orm;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.satyam.orm.dao.Studentdao;
import com.satyam.orm.entities.Student;


/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	ApplicationContext context =new ClassPathXmlApplicationContext("com/satyam/orm/config.xml");
    	Studentdao studentdao =context.getBean("studentdao",Studentdao.class);
    	Student student = new Student(4,"aman","gwalior");
    	
    	int r = studentdao.insert(student);
    	
    	System.out.println("Done"+r);
    }
}
