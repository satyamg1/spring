package com.satyam.orm.dao;

import javax.transaction.Transactional;

import org.hibernate.Hibernate;
import org.springframework.orm.hibernate5.HibernateTemplate;

import com.satyam.orm.entities.Student;

public class Studentdao {
	
	private HibernateTemplate hibernatet;
	
	
	  public HibernateTemplate getHibernatet() {
		return hibernatet;
	}


	public void setHibernatet(HibernateTemplate hibernatet) {
		this.hibernatet = hibernatet;
	}

@Transactional
	public int insert(Student student)
	  {
		  
		  //insert 
	Integer i = (Integer)this.hibernatet.save(student);
	  return i;
	  }
}
