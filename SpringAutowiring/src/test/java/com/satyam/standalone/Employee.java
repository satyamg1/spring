package com.satyam.standalone;

import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

public class Employee {
	
	private List<String> names;
	private Map<String,Integer> course;
	
	
	
	
	

	public Map<String, Integer> getCourse() {
		return course;
	}

	public void setCourse(Map<String, Integer> course) {
		this.course = course;
	}

	public List<String> getNames() {
		return names;
	}

	public void setNames(List<String> names) {
		this.names = names;
	}

	/**
	 * @param names
	 */
	public Employee(List<String> names) {
		super();
		this.names = names;
	}

	/**
	 * 
	 */
	public Employee() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "Employee [names=" + names + ", course=" + course + "]";
	}


    
    
	
    
}
