package com.satyam.standalone;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {

	public static void main(String[] args) {
		
		
	   ApplicationContext context = new ClassPathXmlApplicationContext("com/satyam/standalone/collectionconfig.xml");
		Employee emp12=context.getBean("emp1",Employee.class);

		 System.out.println(emp12);
		 System.out.println(emp12.getNames().getClass().getName())
		 ;
		 
		 System.out.println(emp12.getCourse());
}
}
