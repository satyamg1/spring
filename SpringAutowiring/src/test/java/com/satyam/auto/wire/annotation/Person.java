package com.satyam.auto.wire.annotation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class Person {
	@Autowired
	@Qualifier("address2")
	private Address address;

	/**
	 * 
	 */
	public Person() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param address
	 */
	@Autowired
	public Person(Address address) {
		System.out.println("by constructor");
		this.address = address;
	}

	public Address getAddress() {
		return address;
	}
	
	public void setAddress(Address address) {
		System.out.println("setting address");
		this.address = address;
	}

	@Override
	public String toString() {
		return "Person [address=" + address + "]";
	}

}
