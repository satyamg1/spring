package com.satyam.auto.wire;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {

	public static void main(String[] args) {
		
		
	ApplicationContext context=	new ClassPathXmlApplicationContext("com/satyam/auto/wire/autoconfig.xml");
          Person p1 = context.getBean("p1",Person.class);
          
          System.out.println(p1);
	}

}
