package com.satyam.auto.wire;

public class Person {
	
	private Address address;

	/**
	 * 
	 */
	public Person() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param address
	 */
	public Person(Address address) {
		System.out.println("by constructor");
		this.address = address;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		System.out.println("setting address");
		this.address = address;
	}

	@Override
	public String toString() {
		return "Person [address=" + address + "]";
	}

}
