package com.satyam.jdbc;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import com.satyam.jdbc.dao.Studentdao;
import com.satyam.jdbc.dao.Studentdaoimp;

@Configuration
@ComponentScan(basePackages = {"com.satyam.jdbc.dao"})
public class Jdbcconfig {
	
	@Bean("ds")
	public DriverManagerDataSource getds()
	{
		DriverManagerDataSource ds= new DriverManagerDataSource();
		ds.setDriverClassName("com.mysql.jdbc.Driver");
		ds.setUrl("jdbc:mysql://localhost:3306/jdbc");
		ds.setPassword("Satyam@1234");
		ds.setUsername("root");
		
		return ds;
		
	}
	
	@Bean("jdbctemplate")
	public JdbcTemplate getjdbctemp()
	{
		JdbcTemplate jdbctemp= new JdbcTemplate();
		jdbctemp.setDataSource(getds());
		return jdbctemp;
	}
//	@Bean("studentdaoimp")
//	public Studentdao getdao()
//	{
//		Studentdaoimp sdao= new Studentdaoimp();
//		sdao.setJdbctemplate(getjdbctemp());
//		return sdao;
//	}
	
	
	
	

}
