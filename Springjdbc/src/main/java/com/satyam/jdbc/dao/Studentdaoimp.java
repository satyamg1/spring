package com.satyam.jdbc.dao;

import org.springframework.jdbc.core.JdbcTemplate;

import com.satyam.jdbc.entities.Student;

public class Studentdaoimp implements Studentdao {

	
	private JdbcTemplate  jdbctemplate; 
	
	public JdbcTemplate getJdbctemplate() {
		return jdbctemplate;
	}
	
	public void setJdbctemplate(JdbcTemplate jdbctemplate) {
		this.jdbctemplate = jdbctemplate;
	}

	public int insert(Student student) {
		
		String query= "insert into student(id,name,address) values(?,?,?)";
	int r=	this.jdbctemplate.update(query,student.getId(),student.getName(),student.getAddress());
    return r;
	}

}
