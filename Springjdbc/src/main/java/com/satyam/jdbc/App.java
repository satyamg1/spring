package com.satyam.jdbc;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

import com.satyam.jdbc.dao.Studentdao;
import com.satyam.jdbc.entities.Student;

public class App 
{
    public static void main( String[] args )
    {
       System.out.println("Program Strarted");
      //spring jdbc =>jdbc template
       
       ApplicationContext con= 
    		   new ClassPathXmlApplicationContext("com/satyam/jdbc/config.xml");
       
    	Studentdao studentdao = con.getBean("studentdaoimp",Studentdao.class);
    	//insert
    	
    	Student student = new Student ();
    	
    	student.setAddress("nagpur");
    	student.setId(5);
    	student.setName("aman");
    	
    	int result = studentdao.insert(student);
    	
    	System.out.println("number of recored inserted  "+result);
    	
    	
    	
    	
    	
    }
}
